#+TITLE: State of Guix, 2023 edition

* tech: we’re the best :-)

** Guix Home 🙌

** ~guix shell~, ~--emulate-fhs~

** simplified package inputs

** bootstrapping from 357 bytes!! 😮 (in ~core-updates~, end of 2021)

** qa.guix.gnu.org rocks

** ~guix system image~ awesome

** package transformation options 👍

** UX better than that of Nix (?), still more intimidating than Conda/PyPI (?)

** 22K packages, in the top-10 according to Repology

** ~39K packages if we add guix-cran + guix-emacs :-)

** more channels out there

** ~guix pack~

* community

** more translations (languages and scope)

** blog posts!
** “Ten Years of Guix” in Paris -> success!

  - 60–80 unique visitors :-)
  - sponsorship
  - new faces, different backgrounds

** 10 talks at FOSDEM!!


** Guix is an important (?) major (?!) player in the “open science” space

** scalability issues, dilution of responsibility?

  - release 1.5y after previous one
  - core-updates not merged 1.5y after branching
  - staging, anyone?
  - Rust, antioxydant: what do we do?
  - 40 committers, how many reviewers?
  - how do we help people grow in positions of leadership?
  - how do we facilitate new initiatives?
    + ERIS/P2P substitutes, antioxydant, Guix Home,
      packages.guix.gnu.org, etc.

** facilitation & outreach

  - no GSoC/Outreachy internships since 2019 (?)
    + we’re lacking diversity
  - we need to reach out to other communities:
    + embedded (~guix system image~)
    + devops, sysadmins (~guix system~, ~guix deploy~)
    + container-using people (~guix pack~)
    + self-hosting & autonomy activists (Yunohost, Framasoft & CHATONS)
    + security folks

* the future

** threats 😱

  - Rust! Rust in Linux! Rust everywhereeeeee! we’re doooomed!
  - more generally: growing packaging complexity
  - too many contributions! too few committed volunteers!
    + losing excellent contributions & talented people
  - too many packages! is indefinite growth viable? 🤔
    + 2025: ~guix pull~ requires 10G of RAM and runs in 15mn at best
  - sysadmin doomsday: small bus factor on sysadmin
  - becoming a forever-niche project
  - 2025: keyboard needs proprietary firmware

** opportunities 😍

  - becoming popular!
    + outreach to devops, etc.
    + become *the* reproducible research tool
  - defining a different future
    + Scheme machine?
    + GNU Hurd?
    + frugal computing?
    + human-sized, navigable, empowering operating system?
