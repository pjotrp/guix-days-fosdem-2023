# Guix days 2023 FOSDEM

Dinner Friday - at the Opera.

Rue Grétry 51, 1000 Bruxelles

## Discussion points

### Yesterday

See https://gitlab.com/pjotrp/guix-days-fosdem-2023/

- [X] state of Guix (Ludo)
- [X] Perfect setup/skill sharing (Zimoun)
      + https://gitlab.com/zimoun/my-conf
- [ ] QA (Chris)

Working groups

- [ ] p2p substitutes (pukkamustard)
- [ ] state of the QA (ChrisB)
- [ ] Outreach & internships (Gabor)

### Today

- [ ] Live patch review (ChrisB)

After lunch:

- [ ] Guix Home (Efraim)

Working groups

- [ ] Release management - core updates (Ludo)
      + Guix teams
      + Support multiple versions on trunk? default & stable/unstable
- [ ] risc-v and other non x86 architectures (Efraim)
- [ ] Guix/digital autonomy/programming as a business (Pjotr)


- [ ] Package sizes (bins, docs, dependencies) - Pruning packages?
      + guix pull (Julien - main room)
- [ ] Rust status (Efraim - coffee place)
- [ ] Debugging session at the REPL (jgart - breakout room)




- [ ] reproducible research (Simon)

- nodejs - bundling in Guix
- devop applications
- Guix Home & RDE
- Go status
- Python status
- Patman workflow in Guix?
- Haskell status
- Guix with no internet (apocalyse)
- Hurd updates
- Community building
- Automated container distribution
- Handling secrets in the store
- Road map - is Guix finished?
- Guile Steel, pre-scheme?
- RDF & wikidata
- PHP support
- Guix daemon in Docker
- PXE boot
